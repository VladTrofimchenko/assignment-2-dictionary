%define BUF_SIZE 256
%define CONST_TO_CHANGE_PTR_OF_KEY 8

section .text
%include "colon.inc"
%include "liba.inc"

section .rodata
%include "words.inc"
err: db "The size of string is greater then buffer size",0
err2: db "No word",0

section .text

extern find_word


global _start
_start:
	sub rsp,BUF_SIZE
	mov rdi, rsp		;ptr of key
	mov rsi, BUF_SIZE
	call read_word
	cmp rax, 0
	je .overflow
	mov rsi , first
	mov rdi, rax
	call find_word
	add rsp, BUF_SIZE 
	cmp rax, 0
	je .no_word	
	add rax, CONST_TO_CHANGE_PTR_OF_KEY		;change ptr of key
	push rax 		
	mov rdi, [rsp] 	; adress of ptr -> rdi
	call string_length
	pop rdi		
	add rdi, rax 		;ptr in the end
	inc rdi		; null-term
	call print_stdout_string
	
.end:
	call print_newline
	mov rdi, 0 
	call exit
	

.overflow:
	add rsp, BUF_SIZE
	mov rdi, err
	call print_err
	jmp .end

.no_word:
	mov rdi, err2
	call print_err
	jmp .end
