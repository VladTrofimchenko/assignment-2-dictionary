global find_word
extern string_equals

section .text

find_word:
	xor rax,rax
.loop:
	cmp rsi, 0 
	je .end 
	push rsi
	push rdi
	add rsi, 8  
	call string_equals
	pop rdi
	pop rsi
	cmp rax, 1 
	je .final
	mov rsi, [rsi] 
	jmp .loop
.final:
	mov rax, rsi 
	ret
.end:
	xor rax, rax
	ret
	
