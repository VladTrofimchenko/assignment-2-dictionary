%macro colon 2

%2:
	%ifdef first
		dq first
	%else
		dq 0
	%endif
	db %1, 0
	%define first %2
%endmacro
