AFLAGS=-felf64
ASM=nasm

main: main.o dict.o liba.o
		ld -o main main.o dict.o liba.o

main.o: main.asm colon.inc words.inc
		$(ASM) $(AFLAGS) main.asm

liba.o: liba.asm 
		$(ASM) $(AFLAGS) liba.asm

dict.o: dict.asm 
		$(ASM) $(AFLAGS) dict.asm


clean:
		$(RM) -f main.o liba.o dict.o main

